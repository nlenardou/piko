$(document).ready(function(){
    $('input.filterInput').keyup(function() {
    	var filter = $(this).val();
    	
    	$('table.filterList').find('td.filterOnMe').each(function(){
    		if($(this).text().indexOf(filter) > -1)
    		{
    			$(this).parent().show();
    		}
    		else
    		{
    			$(this).parent().hide();	
    		}
    	});
    });
});