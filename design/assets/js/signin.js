function cryptPassword()
{
	//Récupération du mot de passe
	var pass = document.getElementById('password').value;
	var salt = document.getElementById('salt').value;
	
	//Cryptage du mot de passe en MD5
	var md5_pass = hex_md5(pass);
	var buf = hex_md5(salt + md5_pass);
	
	//Ecriture du mot de passe dans le champ md5
	document.getElementById('md5').value = buf;
}

$(function () {
	
	jQuery.support.placeholder = false;
	test = document.createElement('input');
	if('placeholder' in test) jQuery.support.placeholder = true;
	
	if (!$.support.placeholder) {
		
		$('.field').find ('label').show ();
		
	}
	
});