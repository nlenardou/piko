<?php

/**
 * Signal management
 * @author Nicolas
 */
class SignalCenter
{
    const
        WILDCARD = '*',
        LISTENER = 'listener',
        CALLBACK = 'callback';

    private $signalListeners;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->signalListeners = array();
    }

    /**
     * Emit a signal to listeners
     *
     * @param string $signalId signal to emit
     * @param array parameters to send to listeners callback
     *
     * @return boolean true if some listeners has been warned
     */
    public function emit($signalId, array $params = array())
    {
        if(! $this->isSignalIdValidForEmission($signalId))
        {
            return false;
        }
        
        $listenerInfoArray = $this->getListeners($signalId);

        foreach ($listenerInfoArray as $listenerInfo)
        {
            $this->sendSignal($signalId, $listenerInfo, $params);
        }
        
        return true;
    }

    
    private function sendSignal($signalId, SignalListenerInfo $listenerInfo, array $params)
    {
        array_unshift($params, $signalId);
        
        call_user_func_array($listenerInfo->callback, array($params));
    }

    
    private function getListeners($signalId)
    {
        $listeners = array();
        
        $parts = $this->getSignalIdParts($signalId);
        $candidates = $this->signalListeners;
        
        foreach($parts as $signalPart)
        {
            $stillCandidates = array();
            foreach($candidates as $keyPart => $subArray)
            {
                if( ($keyPart === $signalPart || $keyPart === self::WILDCARD)
                &&  $keyPart !== self::LISTENER)
                {
                    if(isset($candidates[$keyPart][self::LISTENER]))
                    {
                        $listeners = array_merge($listeners, $candidates[$keyPart][self::LISTENER]);
                    }
                    
                    $stillCandidates = array_merge($stillCandidates, $candidates[$keyPart]);
                }
            }
            
            $candidates = $stillCandidates;
        }
        
        return $listeners;
    }

    /**
     * Register a listener and its callback to a given signal
     *
     * @param string $signalId signal to listen
     * @param callback $callback class method to call when signal is emitted
     * @throws SignalRegisterException if callback is not a class method
     */
    public function register($signalId, $callback)
    {
        if (!is_array($callback) || !is_callable($callback))
        {
            throw new SignalRegisterException(
                    'Invalid callback provided for signal ' . $signalId);
        }

        if (!$this->isSignalIdValid($signalId))
        {
            return false;
        }

        return $this->addListener($signalId, $callback);
    }

    private function addListener($signalId, $callback)
    {
        $parts = $this->getSignalIdParts($signalId);

        $current = &$this->signalListeners;
        foreach ($parts as $part)
        {
            if (!array_key_exists($part, $current))
            {
                $current[$part] = array();
            }
            
            $current = &$current[$part];
        }

        if (!array_key_exists(self::LISTENER, $current))
        {
            $current[self::LISTENER] = array();
        }

        $current[self::LISTENER][] = new SignalListenerInfo($callback);
    }

    /**
     * Obtain the listener list for a given signal (include the global listeners)
     *
     * @param string $signalId signal
     * @return array signal listeners
     */
    public function listeners($signalId)
    {
        $listeners = array();
        
        if($this->isSignalIdValid($signalId))
        {
            $listeners = $this->getListeners($signalId);
        }
        
        return $listeners;
    }

    /**
     * Unregister all listener's callbacks to the given signal
     *
     * @param string $signalId signal
     * @param mixed $listener listener to unregister
     *
     * @return boolean if unregistration has been performed (listener found)
     */
    public function unregister($signalId, $listener)
    {
        // FIXME
        $hasUnregistered = false;

        foreach ($this->signalListeners[$signalId] as $index => $listenerInfo)
        {
            if ($listenerInfo[self::LISTENER] === $listener)
            {
                unset($this->signalListeners[$signalId][$index]);
                $hasUnregistered = true;
            }
        }

        return $hasUnregistered;
    }

    private function getSignalIdParts($signalId)
    {
        $parts = explode(':', $signalId);

        return $parts;
    }

    private function isSignalIdValid($signalId)
    {
        $itemIdPattern = '(\w+|\*)'; // difficult to use wilcard constant here
        $pattern = sprintf('~%s(:%s)*~', $itemIdPattern, $itemIdPattern);

        return false !== preg_match($pattern, $signalId);
    }
    
    private function isSignalIdValidForEmission($signalId)
    {
        // * not allowed in emitted signal
        if(strpos($signalId, self::WILDCARD) === false)
        {
            return $this->isSignalIdValid($signalId);
        }
        
        return false;
    }
}
