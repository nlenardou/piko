<?php

class SignalListenerInfo
{
    public
        $listener,
        $callback;
    
    public function __construct(array $callback)
    {
        $this->listener = $callback[0];
        $this->callback = $callback;
    }
}