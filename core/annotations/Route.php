<?php

class Route extends Annotation
{
    const
        INT_PARAMETER    = 'int',
        STRING_PARAMETER = 'str',
        REGEX_DELIMITER = '~';
    
    public
        $controller,
        $method,
        $pattern,
        $regex,
        $parametersValues,
        
        // Automatically filled by Reflection
        $value;
    
    
    public function translate()
    {
        $this->parametersValues = array();
        $this->pattern = $this->value;
        
        if($this->pattern === '/')
        {
            $regex = '/';
        }
        else
        {
            $parts = explode('/', $this->pattern);
            $allowedTypes = array(self::INT_PARAMETER, self::STRING_PARAMETER);
            
            $regex = '';
            
            foreach($parts as $part)
            {
                if(empty($part))
                {
                    continue;
                }
                
                $regex .= '/';
                
                if(preg_match('~\{(?P<param>[^\}]+)\}~', $part, $matches))
                {
                    $param = $matches['param'];
                    $typed = false;
                    
                    if(preg_match('~(?P<type>[^:]+):(?P<param>\w+)~', $param, $matches))
                    {
                        if(in_array($matches['type'], $allowedTypes))
                        {
                            if($matches['type'] === self::INT_PARAMETER)
                            {
                                $typePattern = '\d+';
                            }
                            elseif($matches['type'] === self::STRING_PARAMETER)
                            {
                                $typePattern = '\w+';
                            }
                            
                            $typed = true;
                            //$regex .= sprintf('(?P<%s>%s)', $matches['param'], $typePattern);
                            $regex .= sprintf('(%s)', $typePattern);
                        }
                    }
                    
                    if($typed === false)
                    {
                        //$regex .= sprintf('(?P<%s>[^/]+)', $param);
                        $regex .= '([^/]+)';
                    }
                }
                else
                {
                    $regex .= $part;
                }
            }
        }
        
        $this->regex = sprintf('~^%s$~', $regex);
    }
    
}