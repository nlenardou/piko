<?php

class DebugCenter
{
    private
        $db,
        $request,
        $buffer,
        $enableRecording,
        $startTime;
    
    public function __construct(PdoLayer $db, Request $request)
    {
        $this->db = $db;
        $this->request = $request;
        
        $this->buffer = array();
        $this->startTime = microtime(true);
        $this->enableRecording = true;
    }
    
    private function getTime()
    {
        $now = microtime(true);
        
        return $now - $this->startTime;
    }
    
    public function onStart(array $params)
    {
        $this->startTime = $params['startTime'];
        $this->createTablesIfNeeded();
    }
    
    public function onSignal(array $params)
    {
        if($this->enableRecording === false)
        {
            return;
        }
        
        $signalId = array_shift($params);
        
        $this->buffer[] = array(
            'signalId' => $signalId,
            'params' => $params,
            'time' => $this->getTime() * 1000
        );
    }
    
    public function printDebug(array $params)
    {
        $user = Service::get('user');
        $userId = $user->userId();
        
        $this->db->exec(sprintf(
            "INSERT INTO piko_request (IP, userId, user, uri, real_uri, request_time, duration)
            VALUES ('%s', %s, '%s', '%s', '%s', NOW(), %.3f)",
            $_SERVER['REMOTE_ADDR'],
            empty($userId) ? 'NULL' : intval($userId),
            $user->__toString(),
            $this->request->getOriginalRequestedUrl(),
            $this->request->getRequestedUrl(),
            $this->getTime()
        ));

        $this->enableRecording = false;
        
        $requestId = $this->db->lastInsertID();
        
        foreach($this->buffer as $index => $event)
        {
            $this->db->exec(sprintf(
                "INSERT INTO piko_request_signals (request_id, id, `signal`, params, execution_time)
                VALUES (%d, %d, '%s', '%s', %.3f)",
                $requestId,
                $index + 1,
                $event['signalId'],
                serialize($event['params']),
                $event['time']
            ));
        }
    }

    private function createTablesIfNeeded()
    {
        $queries = array(
<<<SQL
CREATE TABLE IF NOT EXISTS piko_request (
    id int(11) NOT NULL AUTO_INCREMENT,
    IP varchar(15) COLLATE utf8_bin NOT NULL,
    userId int(11) NULL,
    user varchar(64) COLLATE utf8_bin NULL,
    uri varchar(255) COLLATE utf8_bin NOT NULL,
    real_uri varchar(255) COLLATE utf8_bin NOT NULL,
    request_time datetime NOT NULL,
    duration float NOT NULL,
    PRIMARY KEY (id)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;
SQL
, <<< SQL
CREATE TABLE IF NOT EXISTS piko_request_signals (
    request_id int(11) NOT NULL,
    id int(11) NOT NULL,
    signal varchar(255) COLLATE utf8_bin NOT NULL,
    params varchar(512) COLLATE utf8_bin NOT NULL,
    execution_time float NOT NULL,
    PRIMARY KEY (request_id, id)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;
SQL
    );
        
        foreach($queries as $query)
        {
            $this->db->exec($query);
        }
    }
}