<?php

class Response
{
    protected
        $httpCode,
        $contentType,
        $headers,
        $content;
    
    public function __construct($content = '')
    {
        $this->httpCode = 200;
        $this->headers = array();
        $this->contentType = ContentType::HTML;
        $this->content = $content;
    }
    
    
    public function send()
    {
        $this->sendHeaders();
        $this->sendBody();
        
        Piko::emit('response:send', array('response' => $this));
    }
    
    protected function sendHeaders()
    {
        header(
            sprintf('Content-type: %s', $this->contentType),
            true,
            $this->httpCode
        );
        
        foreach($this->headers as $header => $value)
        {
            header(sprintf(
                "%s: %s\n",
                $header,
                $value
            ));
        }
    }
    
    protected function sendBody()
    {
        echo $this->content;
    }
    
    public function setHttpCode($code)
    {
        if(is_numeric($code))
        {
            $this->httpCode = (int) $code;
        }
        
        return $this;
    }
    
    public function getContent()
    {
        return $this->content;
    }
    
    public function setContent($content)
    {
        if(is_string($content))
        {
            $this->content .= $content;
        }
        
        return $this;
    }
    
    public function redirect($uri, $permanent = false)
    {
        $debug = Config::read('piko/Debug/Redirection', false);
        if($debug === true)
        {
            $redirectionHTML = <<<HTML
<hr>
<a href="%s">
    <strong>DEBUG : </strong>redirect to <strong>%s</strong>
</a>
<hr>
HTML;
            $this->setContent(sprintf($redirectionHTML, $uri, $uri));
        }
        else
        {
            $this->httpCode = $permanent ? 301 : 302;
            $this->setHeader('Location', $uri);
        }
    }
    
    public function setHeader($name, $value)
    {
        $this->headers[$name] = $value;
    }
}