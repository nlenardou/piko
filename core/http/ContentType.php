<?php

interface ContentType
{
    const
        HTML = 'text/html',
        XML  = 'text/xml',
        JSON = 'application/json'
    ;
}