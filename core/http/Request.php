<?php

class Request
{
    private
        $flags,
        $session,
        $originalRequestedUrl,
        $requestedUrl;

    
    public function __construct(Session $session)
    {
        $this->originalRequestedUrl = $this->requestedUrl = $_SERVER['REQUEST_URI'];
        $this->initializeSession($session);
        $this->initializeFlags();
    }

    public function getRequestedUrl()
    {
        return $this->requestedUrl;
    }
    
    public function getOriginalRequestedUrl()
    {
        return $this->originalRequestedUrl;
    }
    
    public function setRequestedUrl($url)
    {
        $this->setFlag('rewritten', true);
        $this->requestedUrl = $url;
    }

    private function initializeSession(Session $session)
    {
        $this->session = $session;
        $this->session->begin();
    }

    private function initializeFlags()
    {
        $this->flags = array(
            'rewritten' => false,
            'ajax' => false,
        );
    }

    public function getSession()
    {
        return $this->session;
    }

    public function getFlag($flag)
    {
        if(isset($this->flags[$flag]))
        {
            return $this->flags[$flag];
        }

        Piko::emit('request:flag:read:invalid', array('flag' => $flag));
        
        return null;
    }

    public function setFlag($flag, $value)
    {
        Piko::emit('request:flag:write', array('flag' => $flag, 'value' => $value));
        $this->flags[$flag] = $value;
        
        return $this;
    }
}
