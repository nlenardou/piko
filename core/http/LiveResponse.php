<?php

class LiveResponse extends Response
{
    public function start()
    {
        $this->sendHeaders();
    }
    
    public function send()
    {
        $this->sendBody();
        
        Piko::emit('response:send', array('response' => $this));
    }
}