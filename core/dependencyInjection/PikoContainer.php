<?php

class PikoContainer extends Pimple
{
    public function __construct()
    {
        $this->frameworkInitialize();
        $this->applicationInitialize();
    }
    
    protected function frameworkInitialize()
    {
        $this['signal'] = $this->share(function ($c) {
            return new SignalCenter();
        });
        
        $this['debug'] = $this->share(function ($c) {
           return new DebugCenter($c['db'], $c['request']);
        });
        
        $this['session.name'] = 'pikoSession';
        $this['session.cookie'] = true;
        $this['session'] = $this->share(function ($c) {
           return new Session($c['session.name'], $c['session.cookie']);
        });
        
        $this['user'] = $this->share(function ($c) {
            return new User($c['session']);
        });

        $this['loginForm'] = function ($c) {
            return new LoginForm($c['user'], $c['session']);
        };
        
        $this['cache'] = $this->share(function ($c) {
            return new CacheManager();
        });
        
        $this['request'] = $this->share(function ($c) {
            return new Request($c['session']);
        });
        
        $this['router'] = function ($c) {
            return new Router($c['cache']);
        };
        
        $this['frontController'] = function ($c) {
            return new FrontController($c['router']);
        };
        
        $this['templatePage'] = function ($c) {
            return new TemplatePage($c['templateEngine'], $c['session']);
        };
        
        $this['db'] = $this->share(function ($c) {
            return DatabaseFactory::get();
        });
        
        $this['configuration'] = $this->share(function ($c) {
           return new Config($c['cache']);
        });
        
        $this['AclFilter'] = function ($c) {
          return new AclFilter($c['user']);
        };
        
        $this['AclPrivilegesFilter'] = function ($c) {
          return new AclPrivilegesFilter($c['user'], $c['privileges']);
        };
        
        $this['privileges'] = $this->share(function ($c) {
            return new Privileges($c['user']);
        });
        
        $this['template.debug'] = false;
        $this['template.twig'] = $this->share(function ($c) {
        
            $cacheValue = false;
            if(Config::read('piko/Template/EnableCache', true))
            {
                $cacheValue = CacheManager::DEFAULT_CACHE_DIR . 'twig';
            }
             
            $loader = new Twig_Loader_Filesystem(Config::read('piko/Template/Directories'));
            $twig = new Twig_Environment($loader, array(
                'cache' => $cacheValue,
                'auto_reload' => (bool) Config::read('piko/Template/AutoReload', true),
                'debug' => $c['template.debug'],
            ));
        
            $twig->addExtension(new PikoTwigExtension());
            $twig->addExtension(new AdminTwigExtension());
             
            return $twig;
        });
        
        $this['templateEngine'] = $this->share(function ($c) {
            return new TwigEngine($c['template.twig']);
        });
    }
    
    protected function applicationInitialize()
    {
        
    }
}