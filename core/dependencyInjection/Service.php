<?php

class Service
{
    public static $container = null;
    
    public static function get($serviceName)
    {
        if(is_null(self::$container))
        {
            throw new Exception('Container not initialized');
        }

        return self::$container[$serviceName];
    }
    
    public static function exists($serviceName)
    {
        return isset(self::$container[$serviceName]);
    }
}