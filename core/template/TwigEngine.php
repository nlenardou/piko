<?php

class TwigEngine implements TemplateEngine
{
    private
        $twigEnvironment;
        
    public function __construct(Twig_Environment $twig)
    {
        $this->twigEnvironment = $twig;
    }
    
    public function render($name, array $context = array())
    {
        return $this->twigEnvironment->render($name, $context);
    }
        
    public function addGlobal($name, $value)
    {
        return $this->twigEnvironment->addGlobal($name, $value);
    }
    
    public function getGlobals()
    {
        return $this->twigEnvironment->getGlobals();
    }
}