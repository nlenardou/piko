<?php

class TemplatePage
{
    const
        CSS = 'css',
        JAVASCRIPT = 'js';
    
    private
        $engine,
        $session,
        $variables,
        $template;
    
    public function __construct(TemplateEngine $engine, Session $session)
    {
        $this->variables = array();
        $this->template = null;
        $this->engine = $engine;
        $this->session = $session;
        
        $this->initializeContextVariables();
    }
    
    private function initializeContextVariables()
    {
        $this->addVariable(self::CSS, array());
        $this->addVariable(self::JAVASCRIPT, array());
    }
    
    public function setTemplate($template)
    {
        $this->template = $template;
        
        return $this;
    }
    
    public function render()
    {
        Piko::emit('template:render:start', array('template' => $this->template));
        
        if(is_null($this->template))
        {
            return '';
        }
        
        $this->setFlashes();
        
        $content = $this->engine->render(
            $this->template,
            $this->variables
        );
        
        Piko::emit('template:render:end');
        return $content;
    }
    
    public function addVariable($name, $value)
    {
        $this->variables[$name] = $value;
        
        return $this;
    }
    
    public function addVariables(array $variables)
    {
        $this->variables = array_merge($this->variables, $variables);
        
        return $this;
    }
    
    public function resetVariables()
    {
        $this->variables = array();
        
        return $this;
    }
    
    public function addStylesheet($cssFile)
    {
        $this->variables[self::CSS][] = $cssFile;
        
        return $this;
    }
    
    public function addJavascript($jsFile)
    {
        $this->variables[self::JAVASCRIPT][] = $jsFile;
    
        return $this;
    }
    
    private function setFlashes()
    {
        $this->engine->addGlobal('flashes', $flashes = $this->session->getFlashes());
        
        if(count($flashes) > 0)
        {
            Piko::emit('templates:flashes', array('flashes' => $flashes));
        }
    }
    
}