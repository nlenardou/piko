<?php

interface TemplateEngine
{
	/**
     * Renders a template.
     *
     * @param string $name    The template name
     * @param array  $context An array of parameters to pass to the template
     *
     * @return string The rendered template
     */
    public function render($name, array $context = array());
    
    /**
     * Registers a Global.
     *
     * @param string $name  The global name
     * @param mixed  $value The global value
     */
    public function addGlobal($name, $value);
    
    /**
     * Gets the registered Globals.
     *
     * @return array An array of globals
     */
    public function getGlobals();
}