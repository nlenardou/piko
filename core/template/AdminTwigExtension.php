<?php

class AdminTwigExtension extends Twig_Extension
{
    public function getName()
    {
        return 'admin';
    }
    
    public function getFunctions()
    {
        return array(
            'canAccess' => new Twig_Function_Method($this, 'canAccess'),
        );
    }
    
    public function hasPrivilege($requiredPrivilege)
    {
        $privileges = Service::get('privileges');
        $userPrivileges = $privileges->getUserPrivileges();
        
        return in_array($requiredPrivilege, $userPrivileges);
    }
    
    public function canAccess($route)
    {
        $privileges = Service::get('privileges');
        
        $result = $privileges->canAccess($route);
        return $result & Privileges::GRANTED;
    }
}