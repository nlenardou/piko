<?php

class PikoTwigExtension extends Twig_Extension
{
    public function getName()
    {
        return 'piko';
    }
    
    public function getTests()
    {
        return array(
            'object' => new Twig_Test_Function('is_object'),
            'numeric' => new Twig_Test_Function('is_numeric'),
            'float' => new Twig_Test_Method($this, 'testfloat'),
        );
    }
    
    public function getFunctions()
    {
        return array(
            'var_dump' => new Twig_Function_Function('var_dump'),
        );
    }
    
    public function testFloat($var)
    {
        return is_numeric($var) && strpos($var, '.') !== false;
    }
}