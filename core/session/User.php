<?php

abstract class User
{
    protected
        $session;

    public function __construct(Session $session)
    {
        $this->session = $session;
    }
    
    public function logout()
    {
        $this->session->end();
    }
    
    /**
     * True if the user is connected
     *
     * @return boolean
     */
    abstract public function isConnected();
    abstract public function validateConnection($login, $password, $encryptionClosure = null);
    abstract public function userId();
    abstract public function getPrivileges();
    
    public function __toString()
    {
        return 'User::__toString not implemented';
    }
}