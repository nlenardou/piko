<?php

class Privileges
{
    const
        ROLES_SEPARATOR_CHARACTER = ',',
        
        DENIED = 1,
        NOT_CONNECTED = 3,
        GRANTED = 4,
        PUBLIC_ACCESS = 12
    ;
    
    private
        $user,
        $userPrivilegesCache,
        $lastNeededPrivileges
    ;
    
    public function __construct(User $user)
    {
        $this->user = $user;
        $this->userPrivilegesCache = false;
        $this->lastNeededPrivileges = false;
    }
    
    public function getUserPrivileges()
    {
        if($this->userPrivilegesCache !== false )
        {
            return $this->userPrivilegesCache;
        }
        
        $userPrivileges = $this->user->getPrivileges();
    
        if(! is_array($userPrivileges))
        {
            $userPrivileges = array($userPrivileges);
        }
    
        $userPrivileges = $this->flattenPrivileges($userPrivileges);
    
        return $this->userPrivilegesCache = $userPrivileges;
    }
    
    private function flattenPrivileges(array $privileges)
    {
        $inherits = Config::read('privileges/Roles/Inherits', array());
    
        do
        {
            $stable = true;
            foreach($inherits as $role => $subroles)
            {
                if(in_array($role, $privileges))
                {
                    $subroles = explode(self::ROLES_SEPARATOR_CHARACTER, $subroles);
                    $missingPrivileges = array_diff($subroles, $privileges);
                    $stable &= empty($missingPrivileges);
                    $privileges = array_merge($privileges, $missingPrivileges);
                }
            }
        } while(! $stable);
    
        return array_unique($privileges);
    }
    
    public function canAccess($route)
    {
        $restrictedRoutes = Config::read('privileges/Access/Routes', array());
    
        foreach($restrictedRoutes as $routePattern => $neededPrivileges)
        {
            $regex = sprintf('~^%s~', $routePattern);
            if(preg_match($regex, $route))
            {
                if($this->user->isConnected())
                {
                    return $this->checkPrivileges($neededPrivileges);
                }
                
                return self::NOT_CONNECTED;
            }
        }
        
        return self::PUBLIC_ACCESS;
    }
    
    private function checkPrivileges($neededPrivileges)
    {
        $this->lastNeededPrivileges = $neededPrivileges = explode(Privileges::ROLES_SEPARATOR_CHARACTER, $neededPrivileges);
        $userPrivileges =  $this->getUserPrivileges();
    
        $matches = array_intersect($neededPrivileges, $userPrivileges);
        if(empty($matches))
        {
            return self::DENIED;
        }
    
        return self::GRANTED;
    }
    
    public function getLastNeededPrivileges()
    {
        return $this->lastNeededPrivileges;
    }
}