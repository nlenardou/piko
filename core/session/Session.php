<?php

class Session
{
    
    const
        DEFAULT_SESSION_NAME          = 'framework_session',
        DEFAULT_CSRF_TOKEN_EXPIRATION = 300,
        EXPIRE      = 31536000, /* 3600 * 24 * 365 */
        VAR_INIT    = 'Initiated',
        VAR_IP      = 'IP_ADDR',
        VAR_FLASHES = 'flashes'
    ;

    
    private
        $sessionName,
        $manageCookie,
        $csrfExpiration;
    
    
    public function __construct($sessionName = self::DEFAULT_SESSION_NAME, $manageCookie = true)
    {
        $this->manageCookie = $manageCookie;
        $this->sessionName = $sessionName;
        
        // CSRF prevention
        $this->csrfExpiration = self::DEFAULT_CSRF_TOKEN_EXPIRATION;
    }
    
    public function csrfTokenExpiration()
    {
        return $this->csrfExpiration;
    }
    
    public function begin()
    {
        session_name($this->sessionName);
        session_start();
        
        Piko::emit('session:begin', array('sessionName' => $this->sessionName, 'content' => $_SESSION));
        
        $this->beginCookie();
        $this->preventFixation();
        $this->preventInterception();
    }
    
    
    private function beginCookie()
    {
        if($this->manageCookie)
        {
            if(isset($_COOKIE[$this->sessionName]))
            {
                Piko::emit('session:begin:cookie', array('cookieValue' => $_COOKIE[$this->sessionName]));
            }
        }
    }
    
    
    public function end()
    {
        $_SESSION = array();
        session_destroy();

        $this->endCookie();
        
        Piko::emit('session:end', array('sessionName' => $this->sessionName));
    }
    
    
    private function endCookie()
    {
        if($this->manageCookie)
        {
            $t = time() - 1;
            setcookie($this->sessionName, '', $t);
        }
    }
    
    

    public function save($variable, $value, $saveInCookie = false)
    {
        // save in session
        $_SESSION[$variable] = $value;
        
        // save in cookie
        if($this->manageCookie && $saveInCookie === true)
        {
            $expiration = time() + self::EXPIRE;
            setcookie($this->sessionName, $variable . '=' . $value, $expiration );
        }

        Piko::emit('session:save', array('variable' => $variable, 'value' => $value));
    }
    
    
    public function delete($variable)
    {
        if( isset($_SESSION[$variable]) )
        {
            unset($_SESSION[$variable]);
        }
        
        Piko::emit('session:delete', array('variable' => $variable));
    }
    

    public function exists($variable)
    {
        return (isset ( $_SESSION[$variable] ) && ( ! empty($_SESSION[$variable])) );
    }
    
    
    public function get($variable)
    {
        if( $this->exists($variable) )
        {
            return $_SESSION[$variable];
        }
        
        return null;
    }

    
    private function clean()
    {
        $this->regenerate();
        $_SESSION = array();
    }
    
    
    private function preventFixation()
    {
        $init = $this->get(self::VAR_INIT);
        if($init === null)
        {
            $this->regenerate();
            $this->save(self::VAR_INIT, true);
        }
    }
    
    private function regenerate()
    {
        session_regenerate_id(true);
        Piko::emit('session:regenerate', array('sessionId' => session_id()) );
    }
    
    
    private function preventInterception()
    {
        $remoteIp = $_SERVER['REMOTE_ADDR'];
        $ip = $this->get(self::VAR_IP);
        if($ip !== null)
        {
            if($ip !== $remoteIp)
            {
                $this->clean();
            }
        }
    
        $this->save(self::VAR_IP, $remoteIp);
    }
    
    public function getFlashes()
    {
        $flashes = $this->readFlashes();
        $this->delete(self::VAR_FLASHES);
        
        return $flashes;
    }
    
    private function readFlashes()
    {
        $flashes = array();
        
        if($this->exists(self::VAR_FLASHES))
        {
            $flashes = $this->get(self::VAR_FLASHES);
        }
        
        return $flashes;
    }
    
    public function addFlash(Flash $flash)
    {
        $flashes = $this->readFlashes();
        $flashes[] = $flash;
        
        $this->save(self::VAR_FLASHES, $flashes);
    }
}