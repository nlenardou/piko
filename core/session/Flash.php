<?php

class Flash
{
    const
        SUCCESS = 'success',
        INFO = 'info',
        WARNING = 'warning',
        ERROR = 'error';
    
    public
        $title,
        $message,
        $type;
    
    public function __construct($message, $type = self::NOTICE, $title = null)
    {
        $this->message = $message;
        $this->type = $type;
        $this->title = $title;
    }
    
    public function __toString()
    {
        if(is_null($this->title))
        {
            return $this->displayInline();
        }
        
        return $this->displayAsBlock();
    }
    
    
    private function displayInline()
    {
        
        $code = <<<HTML
<div class="alert alert-%s">
    <button class="close" data-dismiss="alert">×</button>
    %s
</div>
HTML;
        
        return sprintf(
            $code,
            $this->type,
            $this->message
        );
    }
    
    
    private function displayAsBlock()
    {
    
        $code = <<<HTML
<div class="alert alert-%s alert-block">
    <a class="close" data-dismiss="alert" href="#">×</a>
    <h4 class="alert-heading">%s</h4>
    %s
</div>
HTML;
        return sprintf(
                $code,
                $this->type,
                $this->title,
                $this->message
        );
    }
}