<?php

class LoginForm
{
    const
        DEFAULT_LOGIN_ACTION        = '/user/process',
        DEFAULT_LOGIN_FORM_TEMPLATE = 'login.twig',
        DEFAULT_SUBMIT_NAME         = 'submitLogin',
        DEFAULT_SALT                = '12345678951';
        
    
    private
        $user,
        $session,
        $salt,
        $loginAction,
        $loginFormTemplate,
        $submitName;
    
    
    public function __construct(User $user, Session $session)
    {
        $this->user = $user;
        $this->session = $session;
        
        $this->loginAction       = self::DEFAULT_LOGIN_ACTION;
        $this->loginFormTemplate = self::DEFAULT_LOGIN_FORM_TEMPLATE;
        $this->submitName        = self::DEFAULT_SUBMIT_NAME;
        
        $this->initializeSalt();
    }
    
    private function initializeSalt()
    {
        $this->salt = self::DEFAULT_SALT;
        
        $salt = $this->session->get('Salt');
        if($salt !== null)
        {
            $this->salt = $salt;
        }
    }
    
    public function setAction($url)
    {
        if(is_string($url))
        {
            $this->loginAction = $url;
        }
        
        return $this;
    }
    
    public function process($submitName = null)
    {
        Piko::emit('user:login:process:start', array('postData' => $_POST));
        
        if($submitName === null)
        {
            $submitName = $this->submitName;
        }
    
        if(! isset($_POST[$submitName]) )
        {
            return false;
        }
    
        Piko::emit('user:login:process:post');
    
        if($this->detectCsrfAttack())
        {
            return false;
        }
    
        // Posted form is CSRF safe but need to expire token to avoid reuse
        $this->session->save('CsrfTTime_LoginForm', time() - 1);
    
        // check login/passw validity
        $login = $this->ensureAlphanumeric($_POST['username']);
        $passw = $this->ensureMD5($_POST['password']);
        $salt = $this->salt;
        
        $encryptionClosure = function($md5Password) use($salt) {
            return md5($salt . $md5Password);
        };
        
        $valid = $this->user->validateConnection($login, $passw, $encryptionClosure);
        
        Piko::emit('user:login:process:end', array('valid' => $valid));
        return $valid;
    }
    
    
    private function detectCsrfAttack()
    {
        $receivedToken = $this->ensureMD5($_POST['token']);
        $expectedToken = $this->session->get('CsrfToken_LoginForm');
        
        $expiration = $this->session->csrfTokenExpiration();
        $loginFormGenerationTime = $this->session->get('CsrfTTime_LoginForm');
        $loginFormAge = time() - $loginFormGenerationTime;
        
        if($cond1 = ($receivedToken !== $expectedToken)
                || $cond2 = ($loginFormAge > $expiration)
        )
        {
            $explanation = $cond1 ? 'tokens mismatch' : 'token has expired';
            Piko::emit('user:csrf:attack', array(
                    'explanation' =>$explanation,
                    'expectedToken' => $expectedToken,
                    'receivedToken' => $receivedToken,
                    'loginFormAge' => $loginFormAge,
                    'maxAgeAllowed' => $expiration,
            ));
        
            return true;
        }
        
        return false;
    }
    
    private function ensureMD5($variable)
    {
        return preg_replace('~[^a-fA-F0-9]~' , '', $variable);
    }
    
    private function ensureAlphanumeric($variable)
    {
        return preg_replace('~[^\w]~' , '', $variable);
    }
    
    public function render($redirectUriAfterLogin = '/', $formTemplate = null)
    {
        if($formTemplate === null)
        {
            $formTemplate = $this->loginFormTemplate;
        }
    
        $tpl = Service::get('templatePage');
        $tpl->setTemplate($formTemplate)
            ->addVariables($this->prepareLoginForm($redirectUriAfterLogin))
            ->addJavascript('/vendor/piko/piko/design/assets/js/md5.js')
            ->addJavascript('/vendor/piko/piko/design/assets/js/signin.js')
        ;
        
        return $tpl->render();
    }
    
    private function prepareLoginForm($redirectUriAfterLogin = '/')
    {
        // prevent CSRF attack
        $this->generateSalt();
        $token = $this->generateCSRFToken();
        $this->session->save('CsrfToken_LoginForm', $token);
        $this->session->save('CsrfTTime_LoginForm', time());
        $this->session->save('Salt', $this->salt);
    
        $vars = array();
        $vars['csrfToken']  = $token;
        $vars['salt']        = $this->salt;
        $vars['action']      = $this->loginAction;
        $vars['submitName']  = $this->submitName;
        $vars['redirectUri'] = $redirectUriAfterLogin;
    
        return $vars;
    }
    
    private function generateCSRFToken()
    {
        $token = md5(uniqid(rand(), true));
        Piko::emit('user:csrf:generate', array($token));
    
        return $token;
    }
    
    private function generateSalt()
    {
        $this->salt = md5( rand() );
    
        return $this->salt;
    }
}