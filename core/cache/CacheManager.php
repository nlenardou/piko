<?php

class CacheManager
{
    const
        DEFAULT_CACHE_DIR = 'var/cache/';
    
    private
        $canEmitSignals,
        $rootDir;
    
    public function __construct($canEmitSignals = true)
    {
        $this->canEmitSignals = $canEmitSignals;
        $this->rootDir = $this->sanitizeDirectory(self::DEFAULT_CACHE_DIR);
    }
    
    private function sanitizeDirectory($directory)
    {
        if(! is_dir($directory))
        {
            mkdir($directory, 0744, true );
        }
        
        if(substr($directory, -1) !== '/')
        {
            $directory .= '/';
        }
        
        return $directory;
    }
    
    public function getCacheFile($filePath)
    {
        $fileInfo = pathinfo($filePath);

        $fileDirectory = $fileInfo['dirname'];
        if($fileDirectory === '.')
        {
            $fileDirectory = '';
        }
        
        $fileDirectory = $this->sanitizeDirectory(
            $this->rootDir . $fileDirectory
        );
        
        $file = $fileDirectory . $fileInfo['basename'];
        
        return $file;
    }
    
    public function writeFile($filePath, $content)
    {
        $file = $this->getCacheFile($filePath);
        $this->emit('cache:write', array('file' => $filePath, 'content' => $content));
        file_put_contents($file, $content);
    }
    
    public function readFile($filePath, $invalidationTime = 0)
    {
        $file = $this->getCacheFile($filePath);
        
        if(is_file($file))
        {
            if(filemtime($file) > $invalidationTime)
            {
                $content = file_get_contents($file);
                $this->emit('cache:read', array('file' => $filePath, 'content' => $content));
                
                return $content;
            }
        }
        
        return false;
    }
    
    public function purgeFile($filePath)
    {
        $file = $this->getCacheFile($filePath);
        
        if(is_file($file))
        {
            return unlink($file);
        }
        
        return false;
    }
    
    public function emit($signalId, array $params)
    {
        if($this->canEmitSignals === true)
        {
            Piko::emit($signalId, $params);
        }
    }
}