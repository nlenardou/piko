<?php

class AclPrivilegesFilter extends Filter
{
    private
        $user,
        $privileges,
        $restrictedRoutes;
    
    public function __construct(User $user, Privileges $privileges)
    {
        $this->user = $user;
        $this->privileges = $privileges;
        $this->restrictedRoutes = Config::read('privileges/Access/Routes', array());
    }
    
    public function preFilter(Request $request)
    {
        Piko::emit('filter:acl:privileges:check');
        $url = $request->getRequestedUrl();
        
        $status = $this->privileges->canAccess($url);
        
        switch($status)
        {
            case Privileges::NOT_CONNECTED:
                Piko::emit('filter:acl:privileges:denied:notconnected');
                $request->setRequestedUrl(Config::read('acl/Settings/LoginUri', '/'));
                break;
                
            case Privileges::DENIED:
                Piko::emit('filter:acl:privileges:denied', array(
                    'userPrivileges' => $this->privileges->getUserPrivileges(),
                    'neededPrivileges' => $this->privileges->getLastNeededPrivileges()
                ));
                $request->setRequestedUrl(Config::read('privileges/Settings/DeniedUrl', '/'));
                break;
                
            case Privileges::GRANTED:
                Piko::emit('filter:acl:privileges:granted');
                break;
                
            case Privileges::PUBLIC_ACCESS:
                Piko::emit('filter:acl:privileges:granted:public');
                break;
        }
    }
}
