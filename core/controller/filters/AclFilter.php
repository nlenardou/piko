<?php

class AclFilter extends Filter
{
    const
        PUBLIC_MODE = 'public',
        PRIVATE_MODE = 'private';
    
    private
        $user,
        $mode;
    
    public function __construct(User $user)
    {
        $this->user = $user;
        $this->mode = Config::read('acl/Settings/DefaultMode', self::PRIVATE_MODE);
    }
    
    public function preFilter(Request $request)
    {
        if($this->mode === self::PRIVATE_MODE)
        {
            return $this->privateCheck($request);
        }
        
        return $this->publicCheck($request);
    }
    
    private function privateCheck(Request $request)
    {
        Piko::emit('filter:acl:private');
        
        $zones = Config::read('acl/Zones/Public', array());
        $url = $request->getRequestedUrl();
        
        if(! $this->match($url, $zones))
        {
            $excludeZones = Config::read('acl/Zones/Exclude', array());
            if($this->match($url, $excludeZones))
            {
                $this->redirectIfNotConnected($request);
            }
        }
    }
    
    private function publicCheck(Request $request)
    {
        Piko::emit('filter:acl:public');
        
        $zones = Config::read('acl/Zones/Private', array());
        $url = $request->getRequestedUrl();
        
        if($this->match($url, $zones))
        {
            $excludeZones = Config::read('acl/Zones/Exclude', array());
            if(! $this->match($url, $excludeZones))
            {
                $this->redirectIfNotConnected($request);
            }
        }
    }
    
    private function match($url, array $zones)
    {
        foreach($zones as $zone)
        {
            if(preg_match($this->convertZoneToRegex($zone), $url))
            {
                return true;
            }
        }
        
        return false;
    }
    
    private function convertZoneToRegex($zone)
    {
        $zone = str_replace('**', '.+', $zone);
        $zone = str_replace('*', '[^/]+', $zone);
        
        return sprintf('~^%s~U', $zone);
    }
    
    private function redirectIfNotConnected(Request $request)
    {
        Piko::emit('filter:acl:private:access');
        
        if(! $this->user->isConnected())
        {
            $url = Config::read('acl/Settings/LoginUri', '/');
            $request->setRequestedUrl($url);
            
            Piko::emit('filter:acl:private:refused');
        }
        else
        {
            Piko::emit('filter:acl:private:granted');
        }
    }
}