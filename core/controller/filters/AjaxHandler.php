<?php

/**
 * Redirect uri like ajax/<uri> to <uri> and flag Request as ajax one
 */
class AjaxHandler extends Filter
{
    const
        AJAX_PATTERN = '/ajax/';
    
    /**
     * Check if uri starts with 'ajax/' pattern to determine whether the request is an ajax one
     * If it is, rewrite uri without ajax prefix and flag request as an ajax one
     */
    public function preFilter(Request $request)
    {
        $url = $request->getRequestedUrl();
        if(stripos($url, self::AJAX_PATTERN) === 0)
        {
            $request->setRequestedUrl($urlTo = substr($url, strlen(self::AJAX_PATTERN) - 1));
            $request->setFlag('ajax', true);

            Piko::emit('filter:ajax:rewrite', array('urlFrom' => $url, 'urlTo' => $urlTo));
        }
    }

}