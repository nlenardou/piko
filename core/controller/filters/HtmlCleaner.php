<?php

class HtmlCleaner extends Filter
{
    public function postFilter(Request $request, Response $response)
    {
        // if it is not an AJAX request
        if($request->getFlag('ajax') === false)
        {
            Piko::emit('filter:htmlcleaner:start');
            
            // Configuration
            $config = array(
                'indent' => 2,
                'output-xhtml' => true,
                'force-output' => true,
                'quiet' => true,
                'indent-spaces' => 4,
                'new-inline-tags' => 'li',
                'vertical-space' => false,
                'wrap' => 200
            );

            // Tidy
            $tidy = new tidy();
            $tidy->parseString(
                $response->getContent(),
                $config,
                'utf8'
            );
            
            $tidy->cleanRepair();

            Piko::emit('filter:htmlcleaner:end', array('errors' => $tidy->errorBuffer));

            // Affichage
            $response->setContent((string) $tidy);
        }
    }
}
