<?php

abstract class Filter
{
    public function preFilter(Request $request)
    {
    }

    public function postFilter(Request $request, Response $response)
    {
    }
}