<?php

class FrontController
{
    private
        $filters,
        $router;
    
    public function __construct(Router $router)
    {
        $this->router = $router;
        $this->filters = array();
    }
    
    public function addFilter(Filter $filter)
    {
        Piko::emit('controller:filter:add', array('filter' => $filter));
        $this->filters[] = $filter;
    }
    
    /**
     * @param Request $request
     * @return Response
     */
    public function handle(Request $request)
    {
        foreach($this->filters as $filter)
        {
            $filter->preFilter($request);
        }
        
        $response = $this->dispatch($request);
        
        foreach(array_reverse($this->filters) as $filter)
        {
            $filter->postFilter($request, $response);
        }
        
        Piko::emit('controller:response:return', array('response' => $response));
        
        return $response;
    }
    
    /**
     * @param Request $request
     * @return Response
     */
    private function dispatch(Request $request)
    {
        $result = null;
        Piko::emit('request:handle:start', array('request' => $request));
        
        $route = $this->router->getController($request);
        if($route instanceof Route)
        {
            $result = $this->callController($route, $request);
        }
        
        if(is_null($result))
        {
            $result = $this->getDefaultResponse($request);
        }
        
        Piko::emit('request:handle:end');
        return $result;
    }
    
    private function callController(Route $route, Request $request)
    {
        $response = null;
        
        $classname = $route->controller;
        if(class_exists($classname))
        {
            $controller = new $classname();
            if($controller instanceof Controller)
            {
                Piko::emit('request:handle:callController', array('controllerClass' => $classname, 'controllerMethod' => $route->method, 'parameters' => $route->parametersValues));
                $response = $controller->getResponse($request, $route->method, $route->parametersValues);
            }
        }
        
        return $response;
    }
    
    /**
     *
     * @param Request $request
     * @return Response
     */
    private function getDefaultResponse(Request $request)
    {
        $r = new Response();
        $r->setHttpCode(404)
          ->setContent('Page not found');
        
        return $r;
    }
}