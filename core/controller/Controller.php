<?php

abstract class Controller
{
    public function getResponse(Request $request, $method, array $parameters)
    {
        $result = null;
        
        $callback = array($this, $method);
        if(is_callable($callback))
        {
            $result = call_user_func_array($callback, $parameters);
            
            if(! $result instanceof Response)
            {
                $result = new Response($result);
            }
        }
        
        Piko::emit('controller:response:built', array('response' => $result));
        
        return $result;
    }
}