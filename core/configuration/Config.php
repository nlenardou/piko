<?php

class Config
{
    const
        ARRAY_APPEND = '@append',
        CACHE_FILE = 'configuration.serialized',
        CACHE_ERROR_FILE = 'configuration.error.serialized';
    
    private
        $errorFiles,
        $cacheManager,
        $values;

    public static function read($valuePath, $defaultValue = null)
    {
        $config = Service::get('configuration');
        $value =  $config->getValue($valuePath, $defaultValue);
        
        return $value;
    }
    
    private function filterValue($value)
    {
        if(! is_string($value))
        {
            return $value;
        }
        
        $mapping = array(
            'enabled' => true,
            'disabled' => false,
        );
        
        if(array_key_exists($value, $mapping))
        {
            return $mapping[$value];
        }
        
        return $value;
        
    }
    
    public function getAllValues()
    {
        return $this->values;
    }

    public function __construct(CacheManager $cacheManager)
    {
        $this->errorFiles = array();
        $this->cacheManager = $cacheManager;
        $this->loadConfiguration();
    }

    private function getValue($valuePath, $defaultValue)
    {
        $value = $defaultValue;

        if (array_key_exists($valuePath, $this->values))
        {
            $value = $this->values[$valuePath];
        }

        return $value;
    }

    private function loadConfiguration()
    {
        Piko::emit('configuration:load:start');
        
        $values = $this->readCache();

        if($values === null)
        {
            $values = $this->loadConfigurationFromFilesystem();
            $this->writeCache($values);
            $this->writeError();
        }
        
        Piko::emit('configuration:load:end', array('values' => $values));

        $this->values = $values;
    }
    
    private function readCache()
    {
        $values = null;
        
        if(! defined('DISABLE_CONFIGURATION_CACHE'))
        {
            $content = $this->cacheManager->readFile($this->getRelativeCacheFilename());
            
            if($content !== false)
            {
                $values = unserialize($content);
            }
        }
    
        return $values;
    }
    
    public function getCacheFileName()
    {
        return $this->cacheManager->getCacheFile($this->getRelativeCacheFilename());
    }
    
    private function getRelativeCacheFilename()
    {
        return $this->formatCacheFileName(self::CACHE_FILE);
    }
    
    private function formatCacheFileName($fileName)
    {
        return sprintf('%s.%s', ENV, $fileName);
    }
    
    private function writeCache(array $values)
    {
        if(! defined('DISABLE_CONFIGURATION_CACHE'))
        {
            $data = serialize($values);
            $this->cacheManager->writeFile($this->getRelativeCacheFilename(), $data);
        }
    }
    
    private function loadConfigurationFromFilesystem()
    {
        $directories = array(ROOT_DIR . 'settings', 'settings');
        // FIXME first read project & core config and then merge with active plugins only (in order)
        $pluginsDirectories = iterator_to_array(new FilesystemIterator(
            'vendor/piko',
            FilesystemIterator::CURRENT_AS_PATHNAME | FilesystemIterator::SKIP_DOTS
        ));
        $directories += $pluginsDirectories;
        
        $files = array();
        foreach($directories as $directory)
        {
            if($directory === 'vendor/piko' . DIRECTORY_SEPARATOR . 'piko')
            {
                continue;
            }
            
            $foundFiles = $this->findFiles($directory);
            foreach($foundFiles as $fileName => $content)
            {
                if(! isset($files[$fileName]))
                {
                    $files[$fileName] = array();
                }
                
                $files[$fileName][] = $content;
            }
        }
        
        $values = $this->mergeFiles($files);
        $values = $this->overrideWithEnvVariables($values);
        $values = $this->removeAppendDirectives($values);
        
        return $values;
    }
    
    private function findFiles($rootDir)
    {
        $files = array();
    
        $it = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($rootDir));
        foreach ($it as $filePath => $fileInfo)
        {
            if(strpos($filePath, '.ini') === false
            || strpos($filePath, '.svn') !== false
            // OR .<env>.ini but not the current env
            || substr_count($filePath, '.') > 1 && strpos($filePath, '.' . ENV . '.ini') === false
            )
            {
                continue;
            }
            
            $content = @parse_ini_file($filePath, true);
            if($content !== false)
            {
                $key = $fileInfo->getBasename('.ini');
                $files[$key] = $content;
            }
            else
            {
                $this->registerFileError($filePath);
            }
        }
    
        Piko::emit('configuration:files', array('dir' => $rootDir, 'files' => array_keys($files)));
        
        return $files;
    }
    
    private function mergeFiles(array $files)
    {
        $values = array();
        
        foreach($files as $filename => $filesContent)
        {
            $values = array_merge($values, $this->flattenFileValues($filename, $filesContent));
        }

        return $values;
    }
    
    private function flattenFileValues($filename, array $filesContent)
    {
       $values = array();

       foreach($filesContent as $iniValues)
       {
           foreach($iniValues as $group => $variables)
           {
               if(is_array($variables))
               {
                   foreach($variables as $variable => $value)
                   {
                       $completeName = sprintf('%s/%s/%s', $filename, $group, $variable);
                       
                       // override
                       if(array_key_exists($completeName, $values))
                       {
                           $value = $this->mergeValue($values[$completeName], $value);
                       }
                       
                       $values[$completeName] = $this->filterValue($value);
                   }
               }
           }
       }
       
       return $values;
    }
    
    private function mergeValue($value, $override)
    {
        if(is_array($value))
        {
            if(is_array($override))
            {
                return $this->mergeArrayValues($value, $override);
            }
            
            // TODO notice
            return $value;
        }
        
        return $override;
    }
    
    private function mergeArrayValues(array $value, array $override)
    {
        $firstOverrideValue = reset($override);
        
        // append mode
        if($firstOverrideValue !== false && $firstOverrideValue === self::ARRAY_APPEND)
        {
            array_shift($override);
            return array_merge($value, $override);
        }
        
        // override mode
        return $override;
    }
    
    private function overrideWithEnvVariables($values)
    {
        $suffix = '.' . ENV;
        
        $envIndexes = array();
        
        foreach($values as $variable => $value)
        {
            if(strpos($variable, $suffix) !== false)
            {
                $envIndexes[] = $variable;
                $variable = str_replace($suffix, '', $variable);
                
                if(array_key_exists($variable, $values))
                {
                    $value = $this->mergeValue($values[$variable], $value);
                }
                 
                $values[$variable] = $this->filterValue($value);
            }
        }
        
        // Cleaning
        foreach($envIndexes as $index)
        {
            unset($values[$index]);
        }
        
        return $values;
    }

    private function removeAppendDirectives(array $values)
    {
        $filteredValues = array();
        
        foreach($values as $name => $value)
        {
            if(is_array($value))
            {
                $firstItem = reset($value);
                if($firstItem !== false && $firstItem === self::ARRAY_APPEND)
                {
                    array_shift($value);
                }
            }
            
            $filteredValues[$name] = $value;
        }
        
        return $filteredValues;
    }
    
    public function purgeCache()
    {
        $this->cacheManager->purgeFile($this->getRelativeCacheFilename());
    }
    
    private function registerFileError($filePath)
    {
        $this->errorFiles[] = $filePath;
        Piko::emit('configuration:error', array('file' => $filePath));
    }
    
    private function writeError()
    {
        $data = serialize($this->errorFiles);
        $this->cacheManager->writeFile($this->formatCacheFileName(self::CACHE_ERROR_FILE), $data);
    }
    
    public function getErrorFiles()
    {
        $data = $this->cacheManager->readFile(
            $this->formatCacheFileName(self::CACHE_ERROR_FILE)
        );
        
        return unserialize($data);
    }
    
}
