<?php

class PikoInstaller
{
    private
        $endl,
        $root;

    public function __construct()
    {
        $this->endl = PHP_SAPI === 'cli' ? PHP_EOL : '<br/>';
        $this->root = realpath(__DIR__ . DIRECTORY_SEPARATOR . '../../../..') . DIRECTORY_SEPARATOR;
        $this->out('Root dir is ' . $this->root);
    }

    public function run()
    {
        $this->out('Starting installation of missing components');

        $this->createDirectories();
        $this->createIndexFiles();
        $this->createSettingFiles();
        $this->createUserSourceCodeSkeleton();

        $this->out('Installation ended');
    }

    private function createDirectories()
    {
        $directories = array('src', 'settings', 'design', 'design/templates', 'design/assets');

        foreach($directories as $directory)
        {
            $this->makeDirectory($directory);
        }
    }

    private function makeDirectory($dirname)
    {
        $dirpath = $this->root . $dirname;
        if(! is_dir($dirpath))
        {
            $this->out(sprintf('Creating %s', $dirname));
            mkdir($dirpath, 0744, true);
        }
    }
    
    private function createIndexFiles()
    {
        $envs = array('dev', 'prod');
        
        foreach($envs as $env)
        {
            $this->createIndexFile($env);
        }
    }

    private function createIndexFile($env = 'prod')
    {
        $envExtension = '';
        if($env !== 'prod')
        {
            $envExtension = '.' . $env;
        }
        
        $envConstants = '';
        if($env === 'dev')
        {
            $envConstants = <<< PHP
define('DISABLE_CONFIGURATION_CACHE', '');
define('DISABLE_ROUTING_CACHE', '');
PHP;
        }
        
        $filename = sprintf('index%s.php', $envExtension);
        $filepath = $this->root . $filename;

        if(! is_file($filepath))
        {
            $content = <<<PHP
<?php

define('ENV', '$env');
$envConstants;

\$container = function(){
    return new Container();
};

require 'piko/autorun.php';
PHP;
            $this->out(sprintf('Creating %s', $filename));
            file_put_contents($filepath, $content);
        }
    }
    
    private function createSettingFiles()
    {
        $envs = array('dev', 'prod');
        
        foreach($envs as $env)
        {
            $this->createSettingFile($env);
        }
    }
    
    private function createSettingFile($env = 'prod')
    {
        $envExtension = '';
        if($env !== 'prod')
        {
            $envExtension = '.' . $env;
        }
        
        $filename = sprintf('db%s.ini', $envExtension);
        $filepath = $this->root . 'settings/' . $filename;

        if(! is_file($filepath))
        {
            $content = <<<INI
[Server]
Host=127.0.0.1
Port=3306
User=root
Password=
Database=piko
INI;
            $this->out(sprintf('Creating %s', $filename));
            file_put_contents($filepath, $content);
        }
    }
    
    private function createUserSourceCodeSkeleton()
    {
        $containerCode = <<<'PHP'
<?php

class Container extends PikoContainer
{
    protected function applicationInitialize()
    {
        $this['user'] = $this->share( function ($c) {
            return new MyUser($c['session']);
        });
    }
}
PHP;

        $userCode = <<<'PHP'
<?php

class MyUser extends User
{
    public function isConnected()
    {
    	// TODO
        return true;
    }
    
    public function validateConnection($login, $password, $encryptionClosure = null)
    {
    	// TODO
    	return $login === 'fake';
    }
    
    public function userId()
    {
    	// TODO
        return 42;
    }
    
    public function getPrivileges()
    {
        return 'none';
    }
}
PHP;

        $files = array(
            $this->root . 'src/Container.php' => $containerCode,
            $this->root . 'src/MyUser.php' => $userCode,
        );
        
        foreach($files as $filePath => $fileContent)
        {
            if(! is_file($filePath))
            {
                $this->out(sprintf('Creating %s', $filePath));
                file_put_contents($filePath, $fileContent);
            }
        }
    }

    private function out($message)
    {
        echo $message . $this->endl;
    }
}