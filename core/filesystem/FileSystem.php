<?php

class FileSystem
{
    public static function ensureFilePutContents($file, $content)
    {
        $folder = pathinfo($file, PATHINFO_DIRNAME);
        if(! is_dir($folder) )
        {
            if( self::makeDir($folder) === false)
            {
                return false;
            }
        }

        return file_put_contents($file, $content);
    }


    public static function makeDir($directory, $rights = 0755, $recursive = true)
    {
        return mkdir($directory, $rights, $recursive);
    }

    public static function invalidateItem($path, $removeIfNotEmpty = true)
    {
        return self::removeItem($path, $removeIfNotEmpty, false);
    }

    public static function removeItem($path, $removeIfNotEmpty = true, $purge = true)
    {
        if(is_dir($path))
        {
            $subitems = scandir($path);

            if(! $removeIfNotEmpty && count($subitems) > 0)
            {
                return false;
            }

            foreach($subitems as $subitem)
            {
                if($subitem != '.' && $subitem != '..')
                {
                    $subitem = self::sanitizeFolder($path) . $subitem;
                    self::removeItem($subitem, $removeIfNotEmpty, $purge);
                }
            }

            if($purge === true)
            {
                self::removeEmptyDir($path);
            }
        }
        elseif(file_exists($path))
        {
            if($purge === true)
            {
                self::removeFile($path);
            }
            else
            {
                self::invalidateFile($path);
            }
        }
    }

    public static function removeEmptyDir($directory)
    {
        return rmdir($directory);
    }

    public static function removeFile($file)
    {
        return unlink($file);
    }

    public static function invalidateFile($file)
    {
        return touch($file, 0);
    }

    /**
     * Fonction récursive de glob : permet de lister rapidement les fichiers d'un répertoire et de ses sous répertoires.
     *
     * @param comme glob, voir http://fr.php.net/glob
     */
    public static function rglob($pattern, $flags = 0)
    {
        // separator
        $path   = dirname($pattern);
        $pattern_name   = '/' . basename($pattern);

        $return = array();

        // Files
        $return = glob($path . $pattern_name, $flags);

        // Subdirs
        $subdirs        = glob($path . '/*', GLOB_ONLYDIR);
        $nbSubdirs      = count($subdirs);
        for ( $i = 0 ; $i < $nbSubdirs ; $i++ )
        {
            $return = array_merge($return, self::rglob($subdirs[$i] . $pattern_name, $flags));
        }

        return $return;
    }


    public static function sanitizeFolder($folder)
    {
        if(substr($folder, -1) !== DIRECTORY_SEPARATOR)
        {
            $folder .= DIRECTORY_SEPARATOR;
        }

        return $folder;
    }
}