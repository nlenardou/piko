<?php

class HashFolder
{
    const
        DEFAULT_HASH_LEVEL = 1;

    protected
        $baseFolder,
        $hashLevel;


    public function __construct($baseFolder, $hashLevel = self::DEFAULT_HASH_LEVEL)
    {
        parent::__construct(Debug::RUNTIME);

        $this->baseFolder = $baseFolder;
        $this->initBaseFolder();

        $this->hashLevel = self::DEFAULT_HASH_LEVEL;
        if( is_numeric($hashLevel) )
        {
            $this->hashLevel = $hashLevel;
        }
    }


    protected function initBaseFolder()
    {
        $this->baseFolder = FileSystem::sanitizeFolder($this->baseFolder);

        if(! is_dir($this->baseFolder) )
        {
            if( FileSystem::makeDir($this->baseFolder) === false)
            {
                throw new FileException('Unable to create folder' . $this->baseFolder);
            }
        }
    }


    public function retrieveSimpleContentPath($key)
    {
        return $this->retrieveContentPath($this->computeSimpleKeyPath($key));
    }

    public function retrieveComplexContentPath($key, $subkey)
    {
        return $this->retrieveContentPath($this->computeComplexKeyPath($key, $subkey));
    }

    public function readSimpleContent($key)
    {
        return $this->readContent($this->computeSimpleKeyPath($key));
    }

    public function readComplexContent($key, $subkey)
    {
        return $this->readContent($this->computeComplexKeyPath($key, $subkey));
    }


    public function storeSimpleContent($key, $content)
    {
        return $this->storeContent($this->computeSimpleKeyPath($key), $content);
    }

    public function storeComplexContent($key, $subkey, $content)
    {
        return $this->storeContent($this->computeComplexKeyPath($key, $subkey), $content);
    }


    protected function retrieveContentPath($keyPathInfo)
    {
        $file = $this->baseFolder . $keyPathInfo['path'] . $keyPathInfo['file'];

        if(file_exists($file) )
        {
            return $file;
        }
        else
        {
            $this->debug('File does not exist<br><b>' . $file . '</b>', Debug::WARNING);
        }

        return null;
    }


    protected function readContent($keyPathInfo)
    {
        $path = $this->retrieveContentPath($keyPathInfo);
        if($path !== null)
        {
            return file_get_contents($file);
        }

        return null;
    }


    protected function storageFilepath($keyPathInfo)
    {
        $keyPath = $this->baseFolder . $keyPathInfo['path'];

        if(! is_dir($keyPath) )
        {
            FileSystem::makeDir($keyPath);
        }

        return $keyPath . $keyPathInfo['file'];

    }

    protected function storeContent($keyPathInfo, $content)
    {
        $file = $this->storageFilepath($keyPathInfo);
        file_put_contents($file, $content);

        return $file;
    }

    protected function computeComplexKeyPath($key, $subkey)
    {
        $key = md5($key);

        $paths = array();
        for($i = 0 ; $i < $this->hashLevel; $i++)
        {
            $paths[] = $key[0];
            $key = substr($key, 1);
        }

        $paths[] = $key;

        return array(   'path' => implode('/', $paths) . '/',
                'file' => $subkey);
    }

    protected function computeSimpleKeyPath($key)
    {
        $key = md5($key);

        $paths = array();
        for($i = 0 ; $i < $this->hashLevel; $i++)
        {
            $paths[] = $key[0];
            $key = substr($key, 1);
        }

        return array(   'path' => implode('/', $paths),
                'file' => $key);
    }
}