<?php

class Piko
{
    private
        $frontController,
        $pluginsDirectory,
        $startTime;
    
    public function __construct(Closure $containerClosure, array $classes)
    {
        $this->startTime = microtime(true);
        
        $container = $containerClosure();
        $container['classes'] = $classes;
        Service::$container = $container;
        
        $this->pluginsDirectory = rtrim(Config::read('piko/Plugins/Directory', 'vendor/piko'), DIRECTORY_SEPARATOR);
    }
    
    public function run()
    {
        $this->initializeDebug();
        
        self::emit('piko:start');
        
        $this->launch();
    }
    
    
    private function initializeDebug()
    {
        $debug = Service::get('debug');
        $signalCenter = Service::get('signal');
        
        $signalCenter->register('piko:launch', array($debug, 'onStart'));
        
        $signalPatterns = Config::read('piko/Debug/Signals', array('*'));
        foreach($signalPatterns as $signal)
        {
            $signalCenter->register($signal, array($debug, 'onSignal'));
        }
        
        $signalCenter->register('piko:end',    array($debug, 'printDebug'));
    }
    
    private function launch()
    {
        self::emit('piko:launch', array('startTime' => $this->startTime));
        
        $request = Service::get('request');
        $this->frontController = Service::get('frontController');
        
        $this->setControllerFilters();
        
        $response = $this->frontController->handle($request);
        $response->send();
        
        $executionTime = microtime(true) - $this->startTime;
        self::emit('piko:end', array('executionTime' => $executionTime));
    }
    
    private function setControllerFilters()
    {
        $filters = Config::read('piko/Controller/Filters', array());
        
        Piko::emit('controller:filter:init', array('filters' => $filters));
        
        foreach($filters as $filterName)
        {
            $filter = $this->instanciateFilter($filterName);
            
            if($filter instanceof Filter)
            {
                $this->frontController->addFilter($filter);
            }
        }
    }
    
    private function instanciateFilter($filterName)
    {
        $filter = null;
        
        if(Service::exists($filterName))
        {
            $filter = Service::get($filterName);
        }
        elseif(class_exists($filterName))
        {
            $filter = new $filterName();
        }
        
        return $filter;
    }

    public static function emit($signalId, array $params = array())
    {
        $sc = Service::get('signal');
        $sc->emit($signalId, $params);
    }
    
    public static function addFlash(Flash $flash)
    {
        Service::get('session')->addFlash($flash);
    }
}
