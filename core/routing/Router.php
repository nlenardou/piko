<?php

class Router
{
    const
        CACHE_FILE = 'routing.serialized';

    private
        $cacheManager,
        $routes;

    public function __construct(CacheManager $cacheManager)
    {
        Piko::emit('router:init:start');
        
        $this->cacheManager = $cacheManager;

        $routes = $this->readCache();

        if($routes === null)
        {
            $routes = $this->collectRoutes();
            $this->writeCache($routes);
        }

        $this->routes = $routes;

        Piko::emit('router:init:end');
    }

    private function readCache()
    {
        $routes = null;
        
        if(! defined('DISABLE_ROUTING_CACHE'))
        {
            $content = $this->cacheManager->readFile(self::CACHE_FILE);
            
            if($content !== false)
            {
                $routes = unserialize($content);
            }
        }

        return $routes;
    }
    
    public function getCacheFilename()
    {
        return $this->cacheManager->getCacheFile(self::CACHE_FILE);
    }

    private function writeCache(array $routes)
    {
        if(! defined('DISABLE_ROUTING_CACHE'))
        {
            $data = serialize($routes);
            $this->cacheManager->writeFile(self::CACHE_FILE, $data);
        }
    }

    /**
     * @param Request $request
     * @return Route
     */
    public function getController(Request $request)
    {
        $url = $request->getRequestedUrl();

        foreach($this->routes as $route)
        {
            if(preg_match($route->regex, $url, $matches))
            {
                array_shift($matches);
                $route->parametersValues = $matches;

                return $route;
            }
        }

        return null;
    }

    private function collectRoutes()
    {
        $routesAnnotations = array();

        $classes = array_keys(Service::get('classes'));
        $classes = $this->filterClasses($classes);
        
        foreach($classes as $class)
        {
            if(class_exists($class))
            {
                $rClass = new ReflectionAnnotatedClass($class);
                $routesAnnotations = array_merge(
                    $routesAnnotations,
                    $this->getAllAnnotations($rClass)
                );
            }
        }

        return $routesAnnotations;
    }
    
    private function filterClasses(array $classes)
    {
        $excludePatterns = Config::read('piko/Routing/ExcludePathPatterns', array());
        array_walk($excludePatterns, function(&$pattern){
            $pattern = "~$pattern~";
        });
        
        $classes = array_filter($classes, function($path) use($excludePatterns){
            foreach($excludePatterns as $pattern)
            {
                if(preg_match($pattern, $path))
                {
                    return false;
                }
            }
            
            return true;
        });
        
        return $classes;
    }

    private function getAllAnnotations(ReflectionAnnotatedClass $rClass)
    {
        $routesAnnotations = array();

        if ($rClass->isSubclassOf('Controller'))
        {
            $methods = $rClass->getMethods();
            foreach ($methods as $method)
            {
                $annotations = $method->getAllAnnotations('Route');
                foreach($annotations as $annotation)
                {
                    $annotation->controller = $rClass->getName();
                    $annotation->method = $method->getName();
                    $annotation->translate();
                }

                $routesAnnotations = array_merge($routesAnnotations, $annotations);
            }
        }

        return $routesAnnotations;
    }

    public function getRoutes()
    {
        return $this->routes;
    }
    
    public function purgeCache()
    {
        $this->cacheManager->purgeFile(self::CACHE_FILE);
    }
}
