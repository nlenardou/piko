<?php

abstract class PdoLayer
{
    protected
        $queriesCounter,
        $queriesTime,
        $pdo;
    
    abstract protected function dsn($host, $port, $db = null);
    
    public function __construct($host, $port, $login, $password, $db = null)
    {
        $this->connect($host, $port, $login, $password, $db);
        $this->queriesCounter = $this->queriesTime = 0;
    }
    
    private function connect($host, $port, $login, $password, $db = null)
    {
        $dsn = $this->dsn($host, $port, $db);
        
        try
        {
            $this->pdo = new PDO($dsn, $login, $password);
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
            if($db !== null)
            {
                $this->selectDB($db);
            }
        }
        catch(PDOException $e)
        {
            Piko::emit('db:connect:error', array(
                'dsn' => $dsn,
                'login' => $login,
                'password' => $password,
                'exception' => $e,
            ));
            
            throw $e;
        }
        
        Piko::emit('db:connect:success', array(
            'dsn' => $dsn,
            'login' => $login,
            'password' => $password,
        ));
    }
    
    public function selectDB($database)
    {
        Piko::emit('db:select', array('database' => $database));
        
        return $this->exec('USE ' . $database . ';');
    }
    
    public function query($selectQuery)
    {
        $t1 = microtime(true);
        $statement = $this->pdo->query($selectQuery);
        $t2 = microtime(true);
        
        $this->queriesCounter++;
        $this->queriesTime += $t2 - $t1;

        Piko::emit('db:query', array(
            'query' => $selectQuery,
            'rowCount' => $statement->rowCount(),
            'executionTime' => $t2 - $t1,
        ));
        
        return $statement;
    }
    
    public function exec($sqlStatement)
    {
        $nbRows = false;
        
        try
        {
            $t1 = microtime(true);
            $nbRows = $this->pdo->exec($sqlStatement);
            $t2 = microtime(true);

            $this->queriesCounter++;
            $this->queriesTime += $t2 - $t1;

            Piko::emit('db:query:success', array(
                'query' => $sqlStatement,
                'rowCount' => $nbRows,
                'executionTime' => $t2 - $t1,
            ));
        }
        catch(PDOException $e)
        {
            Piko::emit('db:query:error', array(
                'query' => $sqlStatement,
            ));
        }
            
        return $nbRows;
    }
    
    public function begin()
    {
        Piko::emit('db:transaction:begin');
        
        return $this->pdo->beginTransaction();
    }
    
    public function commit()
    {
        Piko::emit('db:transaction:commit');
        
        return $this->pdo->commit();
    }
    
    public function rollback()
    {
        Piko::emit('db:transaction:rollback');
        
        return $this->pdo->rollBack();
    }
    
    public function executeFile($sqlFile)
    {
        if(! is_file($sqlFile))
        {
            Piko::emit('db:execfile:error', array('error' => 'file no found', 'file' => $sqlFile));
            return false;
        }
        
        $sql        = file_get_contents($sqlFile);
        $queries    = explode(';', $sql);
        
        Piko::emit('db:execfile:start', array('file' => $sqlFile, 'queries' => $queries));

        foreach($queries as $q)
        {
            $q = trim($q);
            if(! empty($q))
            {
                $this->query($q);
            }
        }
                    
        Piko::emit('db:execfile:end', array('file' => $sqlFile));
    }

    public function prepare($query)
    {
        return $this->pdo->prepare($query);
    }

    public function lastInsertID()
    {
        return $this->pdo->lastInsertId();
    }
}