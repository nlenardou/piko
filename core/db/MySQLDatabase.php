<?php

class MySQLDatabase extends PdoLayer
{
    private
        $dsn;

    protected function dsn($host, $port, $db = null)
    {
        $dsn = sprintf(
            'mysql:host=%s;port=%d',
            $host,
            $port
        );
        
        if($db !== null)
        {
            $dsn .= sprintf(
                ';database=%s',
                $db
            );
        }
         
        return $dsn;
    }
}