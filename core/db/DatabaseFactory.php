<?php

class DatabaseFactory
{
    const
        HOST = 'db/Server/Host',
        PORT = 'db/Server/Port',
        USER = 'db/Server/User',
        PASS = 'db/Server/Password',
        DATABASE = 'db/Server/Database';
    
    public static function get()
    {
        $driver = Config::read('db/Server/Driver', 'mysql');
        
        switch($driver)
        {
            case 'mysql':
                return new MySQLDatabase(
                    Config::read(self::HOST),
                    Config::read(self::PORT),
                    Config::read(self::USER),
                    Config::read(self::PASS),
                    Config::read(self::DATABASE, null)
                );
        }
        
        return null;
    }
}