<?php

define('ROOT_DIR', 'vendor/piko/piko/');

// ----------------------------------------------------------------------------
// Autoloading
// ----------------------------------------------------------------------------
require 'vendor/autoload.php';

// ----------------------------------------------------------------------------
// Bootstrap
// ----------------------------------------------------------------------------
if(! isset($container) || ! is_callable($container))
{
    $container = function () {
        return new PikoContainer();
    };
}

$classes = include 'vendor/composer/autoload_classmap.php';

$piko = new Piko($container, $classes);
$piko->run();
