<?php

class PikoConfiguration extends PikoAdminController
{
    
    /**
     * @Route('/admin/config')
     */
    public function config()
    {
        $config = Service::get('configuration');
        $values = $config->getAllValues();
        ksort($values);
    
        $tpl = Service::get('templatePage');
    
        $tpl->setTemplate('config.twig')
            ->addVariable('title', 'Configuration')
            ->addVariable('variables', $values)
            ->addVariable('cacheFile', $config->getCacheFileName())
            ->addVariable('errorFiles', $config->getErrorFiles())
            ->addJavascript(self::ASSETS_URL . 'js/filterList.js')
        ;
    
        return $tpl->render();
    }
    
    /**
     * @Route('/admin/config/purge')
     */
    public function purgeConfig()
    {
        $config = Service::get('configuration');
        $config->purgeCache();
    
        Piko::addFlash(new Flash('Configuration cache purged', Flash::SUCCESS));
    
        $r = new Response();
        $this->redirectToRefererIfPossible($r);
    
        return $r;
    }
}