<?php

class PikoAdmin extends PikoAdminController
{
    /**
     * @Route('/admin/framework')
     */
    public function home()
    {
        $tpl = Service::get('templatePage');
        
        $tpl->setTemplate('framework.twig');
        
        return $tpl->render();
    }
    
    /**
     * @Route('/admin/caches/purgeall')
     */
    public function purgeAllCaches()
    {
        $messages = array();
     /*
        Service::get('autoloader')->purgeCache();
        $messages[] = 'Autoloading cache purged';
       //*/
        Service::get('configuration')->purgeCache();
        $messages[] = 'Configuration cache purged';
        
        Service::get('router')->purgeCache();
        $messages[] = 'Routing cache purged';
    
        Piko::addFlash(new Flash(implode('<br/>', $messages), Flash::SUCCESS));
    
        $r = new Response();
        $this->redirectToRefererIfPossible($r);
    
        return $r;
    }
}