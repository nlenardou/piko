<?php

abstract class PikoAdminController extends Controller
{
    const
        ADMIN_HOME_URL = '/admin/framework',
        ASSETS_URL = '/vendor/piko/piko/design/assets/';
    
    protected
        $engine,
        $user;
    
    public function __construct()
    {
        $this->engine = Service::get('templateEngine');
        $this->user = Service::get('user');
        
        $connected = $this->user->isConnected();
        $this->engine->addGlobal('userConnected', $connected);
        if($connected)
        {
            $this->engine->addGlobal('userName', (string) $this->user);
        }
    }
    
    protected function redirectToRefererIfPossible(Response $response)
    {
        $redirectionUrl = self::ADMIN_HOME_URL;
        
        if(isset($_SERVER['HTTP_REFERER']))
        {
            $redirectionUrl = parse_url($_SERVER['HTTP_REFERER'], PHP_URL_PATH);
        }
        
        $request = Service::get('request');
        
        if($redirectionUrl !== $request->getRequestedUrl())
        {
            $response->redirect($redirectionUrl);
            
            return true;
        }
        
        return false;
    }
}