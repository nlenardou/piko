<?php

class PikoUserController extends Controller
{
    /**
     * @Route('/user/login')
     */
    public function login()
    {
        $request = Service::get('request');
        $loginForm = Service::get('loginForm');
        
        $redirectUrl = '/';
        $requestedUrl = $request->getOriginalRequestedUrl();
        if($requestedUrl != '/user/login')
        {
            $redirectUrl = $requestedUrl;
        }
        
        return $loginForm->render($redirectUrl);
    }
    
    /**
     * @Route('/user/process')
     */
    public function loginCheck()
    {
        $response = new Response();
        
        $loginForm = Service::get('loginForm');
        if(! $loginForm->process())
        {
            Piko::addFlash(new Flash('Invalid login or password', Flash::ERROR));
        }
        
		$url = isset($_POST['redirectUri']) ? $_POST['redirectUri'] : '/user/login';
        $response->redirect($url);
        
        return $response;
        
    }
    
    /**
     * @Route('/user/logout')
     */
    public function logout()
    {
        $user = Service::get('user');
        $user->logout();
        
        $response = new Response();
        $response->redirect('/');
    
        return $response;
    }
    
    /**
     * @Route('/user/denied')
     */
    public function denied()
    {
        $tpl = Service::get('templatePage');
        
        $tpl->setTemplate('denied.twig');
        
        $response = new Response();
        $response->setContent($tpl->render())
                 ->setHttpCode(403);
        
        return $response;
    }
}