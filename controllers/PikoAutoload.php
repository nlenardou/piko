<?php

class PikoAutoload extends PikoAdminController
{
    /**
     * @Route('/admin/autoload')
     */
    public function autoloading()
    {
        $classes = Service::get('classes');
        ksort($classes);
    
        $tpl = Service::get('templatePage');
    
        $tpl->setTemplate('autoloads.twig')
            ->addVariable('title', 'Autoloads')
            ->addVariable('classes', $classes)
            ->addJavascript(self::ASSETS_URL . 'js/filterList.js')
        ;
    
        return $tpl->render();
    }
    
    /**
     * @Route('/admin/autoload/purge')
     */
    public function purgeAutoload()
    {/*
        $autoloader = Service::get('autoloader');
        $autoloader->purgeCache();
        Piko::addFlash(new Flash('Autoloading cache purged', Flash::SUCCESS));
    //*/
        $r = new Response();
        $this->redirectToRefererIfPossible($r);
    
        return $r;
    }
    
    
}