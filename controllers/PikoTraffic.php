<?php

class PikoTraffic extends PikoAdminController
{
    /**
     * @Route('/admin/traffic')
     */
    public function traffic()
    {
        $tpl = Service::get('templatePage');
    
        $db = Service::get('db');
        $st = $db->query('SELECT * FROM piko_request ORDER BY id DESC LIMIT 50');
        $items = $st->fetchAll();
    
        $tpl->setTemplate('traffic.twig')
            ->addVariable('title', 'Traffic')
            ->addVariable('requests', $items)
            ->addJavascript(self::ASSETS_URL . 'js/filterList.js')
        ;
    
        return $tpl->render();
    }
    
    /**
     * @Route('/admin/traffic/{int:requestId}')
     */
    public function trafficDetail($requestId)
    {
        $tpl = Service::get('templatePage');
    
        $db = Service::get('db');
        
        $st = $db->query(sprintf('SELECT * FROM piko_request WHERE id = %d', $requestId));
        $request = $st->fetch();
        
        $st = $db->query(sprintf(
            "SELECT * FROM piko_request_signals WHERE request_id = %d ORDER BY id ASC",
            $requestId
        ));
        $items = $st->fetchAll(PDO::FETCH_FUNC, array($this, 'convertRequestSignalRow'));
    
        $tpl->setTemplate('trafficDetailed.twig')
            ->addVariable('title', 'traffic')
            ->addVariable('signals', $items)
            ->addVariable('requestId', $requestId)
            ->addVariable('request', $request)
            ->addJavascript(self::ASSETS_URL . 'js/filterList.js')
        ;
    
        return $tpl->render();
    }
    
    public function convertRequestSignalRow($requestId, $order, $signal, $data, $time)
    {
        static $oldTime = 0;
    
        $row = array(
            'order' => $order,
            'signal' => $signal,
            'data' => unserialize($data),
            'time' => $time,
            'duration' => sprintf('%.3f', $time - $oldTime)
        );
    
        $oldTime = $time;
    
        return $row;
    }
    
    /**
     * @Route('/admin/traffic/{int:requestId}/{int:order}')
     */
    public function trafficDetailOneSignal($requestId, $order)
    {
        $tpl = Service::get('templatePage');
    
        $db = Service::get('db');
        $st = $db->query(sprintf(
            "SELECT * FROM piko_request_signals WHERE request_id = %d AND id = %d",
            $requestId,
            $order
        ));
        $row = $st->fetch();
    
        if($row !== false)
        {
            $row = $this->convertRequestSignalRow(
                $row['request_id'],
                $row['id'],
                $row['signal'],
                $row['params'],
                $row['execution_time']
            );
        }
    
        $tpl->setTemplate('trafficSignal.twig')
            ->addVariable('title', 'Traffic')
            ->addVariable('requestId', $requestId)
            ->addVariable('signalId', $order)
            ->addVariable('row', $row)
        ;
    
        return $tpl->render();
    }
}