<?php

class PikoRouting extends PikoAdminController
{
    /**
     * @Route('/admin/routing')
     */
    public function routing()
    {
        $router = Service::get('router');
        $routes = $router->getRoutes();
        sort($routes);
    
        $tpl = Service::get('templatePage');
    
        $tpl->setTemplate('routing.twig')
            ->addVariable('title', 'Routing')
            ->addVariable('routes', $routes)
            ->addVariable('cacheFile', $router->getCacheFilename())
            ->addJavascript(self::ASSETS_URL . 'js/filterList.js');
        ;
    
        return $tpl->render();
    }
    
    
    /**
     * @Route('/admin/routing/purge')
     */
    public function purgeRouting()
    {
        Service::get('router')->purgeCache();
        Piko::addFlash(new Flash('Routing cache purged', Flash::SUCCESS));

        $response = new Response();
        $this->redirectToRefererIfPossible($response);
    
        return $response;
    }
}